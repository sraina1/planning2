/*=================================================================
 *
 * planner.c
 *
 *=================================================================*/
#include <math.h>
#include "mex.h"
#include <time.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

/* Input Arguments */
#define	MAP_IN      prhs[0]
#define	ARMSTART_IN	prhs[1]
#define	ARMGOAL_IN     prhs[2]


/* Output Arguments */
#define	PLAN_OUT	plhs[0]
#define	PLANLENGTH_OUT	plhs[1]

#define GETMAPINDEX(X, Y, XSIZE, YSIZE) (Y*XSIZE + X)

#if !defined(MAX)
#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define	MIN(A, B)	((A) < (B) ? (A) : (B))
#endif

#define PI 3.141592654

//the length of each link in the arm (should be the same as the one used in runtest.m)
#define LINKLENGTH_CELLS 10

typedef struct {
  int X1, Y1;
  int X2, Y2;
  int Increment;
  int UsingYIndex;
  int DeltaX, DeltaY;
  int DTerm;
  int IncrE, IncrNE;
  int XIndex, YIndex;
  int Flipped;
} bresenham_param_t;



void ContXY2Cell(double x, double y, short unsigned int* pX, short unsigned int *pY, int x_size, int y_size)
{
    double cellsize = 1.0;
	//take the nearest cell
	*pX = (int)(x/(double)(cellsize));
	if( x < 0) *pX = 0;
	if( *pX >= x_size) *pX = x_size-1;

	*pY = (int)(y/(double)(cellsize));
	if( y < 0) *pY = 0;
	if( *pY >= y_size) *pY = y_size-1;
}


void get_bresenham_parameters(int p1x, int p1y, int p2x, int p2y, bresenham_param_t *params)
{
  params->UsingYIndex = 0;

  if (fabs((double)(p2y-p1y)/(double)(p2x-p1x)) > 1)
    (params->UsingYIndex)++;

  if (params->UsingYIndex)
    {
      params->Y1=p1x;
      params->X1=p1y;
      params->Y2=p2x;
      params->X2=p2y;
    }
  else
    {
      params->X1=p1x;
      params->Y1=p1y;
      params->X2=p2x;
      params->Y2=p2y;
    }

   if ((p2x - p1x) * (p2y - p1y) < 0)
    {
      params->Flipped = 1;
      params->Y1 = -params->Y1;
      params->Y2 = -params->Y2;
    }
  else
    params->Flipped = 0;

  if (params->X2 > params->X1)
    params->Increment = 1;
  else
    params->Increment = -1;

  params->DeltaX=params->X2-params->X1;
  params->DeltaY=params->Y2-params->Y1;

  params->IncrE=2*params->DeltaY*params->Increment;
  params->IncrNE=2*(params->DeltaY-params->DeltaX)*params->Increment;
  params->DTerm=(2*params->DeltaY-params->DeltaX)*params->Increment;

  params->XIndex = params->X1;
  params->YIndex = params->Y1;
}

void get_current_point(bresenham_param_t *params, int *x, int *y)
{
  if (params->UsingYIndex)
    {
      *y = params->XIndex;
      *x = params->YIndex;
      if (params->Flipped)
        *x = -*x;
    }
  else
    {
      *x = params->XIndex;
      *y = params->YIndex;
      if (params->Flipped)
        *y = -*y;
    }
}

int get_next_point(bresenham_param_t *params)
{
  if (params->XIndex == params->X2)
    {
      return 0;
    }
  params->XIndex += params->Increment;
  if (params->DTerm < 0 || (params->Increment < 0 && params->DTerm <= 0))
    params->DTerm += params->IncrE;
  else
    {
      params->DTerm += params->IncrNE;
      params->YIndex += params->Increment;
    }
  return 1;
}



int IsValidLineSegment(double x0, double y0, double x1, double y1, double*	map,
		   int x_size,
 		   int y_size)

{
	bresenham_param_t params;
	int nX, nY; 
    short unsigned int nX0, nY0, nX1, nY1;

    //printf("checking link <%f %f> to <%f %f>\n", x0,y0,x1,y1);
    
	//make sure the line segment is inside the environment
	if(x0 < 0 || x0 >= x_size ||
		x1 < 0 || x1 >= x_size ||
		y0 < 0 || y0 >= y_size ||
		y1 < 0 || y1 >= y_size)
		return 0;

	ContXY2Cell(x0, y0, &nX0, &nY0, x_size, y_size);
	ContXY2Cell(x1, y1, &nX1, &nY1, x_size, y_size);

    //printf("checking link <%d %d> to <%d %d>\n", nX0,nY0,nX1,nY1);

	//iterate through the points on the segment
	get_bresenham_parameters(nX0, nY0, nX1, nY1, &params);
	do {
		get_current_point(&params, &nX, &nY);
		if(map[GETMAPINDEX(nX,nY,x_size,y_size)] == 1)
            return 0;
	} while (get_next_point(&params));

	return 1;
}

int IsValidArmConfiguration(double* angles, int numofDOFs, double*	map,
		   int x_size, int y_size)
{
    double x0,y0,x1,y1;
    int i;
    
 	//iterate through all the links starting with the base
	x1 = ((double)x_size)/2.0;
    y1 = 0;
	for(i = 0; i < numofDOFs; i++)
	{
		//compute the corresponding line segment
		x0 = x1;
		y0 = y1;
		x1 = x0 + LINKLENGTH_CELLS*cos(2*PI-angles[i]);
		y1 = y0 - LINKLENGTH_CELLS*sin(2*PI-angles[i]);

		//check the validity of the corresponding line segment
		if(!IsValidLineSegment(x0,y0,x1,y1,map,x_size,y_size))
				return 0;
	}    
  return 1;
}


class Node 
{
public:
  std::vector<double> v;
  int parent;
  double cost;

  bool conn_s;
  bool conn_g;
  
  std::vector<int> nbrs;

  bool visited = false;
  bool g_val = false;
  int h = -1;

  Node* pNode;

  Node(){}
  Node(std::vector<double> v) 
  {
    this -> v = v;
  }
  Node(std::vector<double> v, int parent) 
  {
    this -> v = v;
    this -> parent = parent;
  }
};

void print_config(std::vector<double> v) 
{
  std::cout<<std::endl<<std::endl;
  for(auto a : v) {
    std::cout<<a<<" ";
  }
  std::cout<<std::endl<<std::endl;
}



double distance(const std::vector<double>& a, const std::vector<double>& b)
{
  double dist = 0;
  for(int i = 0; i < a.size(); i++){
    dist+= pow(a[i]-b[i],2);
  }
  return sqrt(dist);

}

struct neighbor{
  int index;
  double distance;
};

neighbor* nearest_neighbors(const std::vector<double>& v, const std::vector<Node*>& T)
{
  double min_dist = distance(v,T[0]->v);
  int index = 0;
  for(int i = 1; i < T.size(); i++){
    if(distance(v,T[i]->v) < min_dist) {
      min_dist = distance(v,T[i]->v);
      index = i;
    }
  }
  neighbor* n = new neighbor();
  n -> distance = min_dist;
  n -> index = index;
  return n;
}

std::vector<double> new_config(const std::vector<double>& v1, const std::vector<double>& v2,
                              double dist, double eps) 
{
  std::vector<double> nc(v1.size());
  for(int i = 0; i <v1.size(); i++) {
    nc[i] = v1[i] + eps * ((v2[i] - v1[i]) / dist);
  }
  return nc;

}

bool extend(const std::vector<double>& v1, const std::vector<double>& v2, double dist,
            double *map, int x_size, int y_size)
{
  double res = PI/20;
  int jumps = int(dist/res)+1;
  double* temp = (double*) malloc(v1.size() * sizeof(double));
  for(int i = 0; i < jumps; i++){
    for (int j = 0; j < v1.size(); j++) {
      temp[j] = v1[j] + double(i+1)*res*((v2[j]-v1[j])/dist);
    }
    if(IsValidArmConfiguration(temp, v1.size(), map, x_size, y_size)==0)
        return false;
  }
  return true;
}

std::vector<int> backtrack(std::vector<Node*> RRTTree)
{
  std::vector<int> plan;
  int i = RRTTree.size() - 1;
  while(i != -1 ){
    plan.push_back(i);    
    i = RRTTree[i] -> parent;
  }
  std::reverse(plan.begin(),plan.end()); 
  return plan;

}

std::vector<int> fwdtrack(std::vector<Node*> RRTTree)
{
  std::vector<int> plan;
  int i = RRTTree.size() - 1;
  while(i != -1 ){
    plan.push_back(i);    
    i = RRTTree[i] -> parent;
  }
  return plan;

}





double* vec_to_dbl(std::vector<double> v)
{
  double* d =  (double*) malloc(v.size()*sizeof(double));
  for(int i =0; i < v.size(); i++){
    d[i] = v[i];
  }

  return d;
}



static void planner1(
		   double*	map,
		   int x_size,
 		   int y_size,
           double* armstart_anglesV_rad,
           double* armgoal_anglesV_rad,
	   int numofDOFs,
	   double*** plan,
	   int* planlength)
{
	//no plan by default
	*plan = NULL;
	*planlength = 0;

  std::vector<double> start_pos, goal_pos;

  for (int i = 0; i < numofDOFs; i++) {
    start_pos.push_back(armstart_anglesV_rad[i]);
    goal_pos.push_back(armgoal_anglesV_rad[i]);
  }

  Node* start = new Node(start_pos, -1);
  Node* goal = new Node(goal_pos);
  std::vector<Node*> RRTTree;
  RRTTree.push_back(start);
  //RRT

  int iter = 0;
  double eps = PI/2;
  bool GOALFLAG = false;
  std::vector<double> rc(numofDOFs);
  while(1) {
    // GENERATE RANDOM CONFIGURATION
    if(iter % 4 == 0){
      rc = goal_pos;
      GOALFLAG = true;
    }
    else {
      srand(iter);
      for (int i = 0; i < numofDOFs; i++) {
        rc[i] = (rand() / (RAND_MAX/(2 * PI )));
        }
        GOALFLAG = false;
      }
      // if(!IsValidArmConfiguration(rc, numofDOFs, map, x_size, y_size))
      //   continue;
      iter++;
      
      // GET NEAREST NEIGHBOR AND NEW CONFIG
      neighbor* n = nearest_neighbors(rc,RRTTree); 
      if(n-> distance > eps) {
        GOALFLAG = false;
        rc = new_config(RRTTree[n->index]->v, rc, n-> distance, eps);
        n->distance = eps;
      }
      //EXTEND
      if(extend(RRTTree[n->index]->v, rc, n-> distance, map, x_size, y_size)){
        Node* config = new Node(rc,n->index);
        RRTTree.push_back(config);
        if(GOALFLAG==true)
          break;
        if(distance(rc,goal_pos)==0) break;
      }
      if(iter%1000==0)
        // std::cout<<"ITERATION: "<<iter<<"\n";
      if(iter % 1000000 == 0){
        break;
      }
  }


  double PQ = 0;

  std::vector<int> planned_path =  backtrack(RRTTree);
  int firstinvalidconf = 1;
    *plan = (double**) malloc(planned_path.size()*sizeof(double*));
        for (int i = 0; i < planned_path.size(); i++){
            (*plan)[i] = (double*) malloc(numofDOFs*sizeof(double)); 
            for(int j = 0; j < numofDOFs; j++){
              (*plan)[i][j] = RRTTree[planned_path[i]] -> v[j];
            }           

            if(i>0){
                PQ+= distance(RRTTree[planned_path[i-1]] -> v, RRTTree[planned_path[i]] -> v);
              }
        if(!IsValidArmConfiguration((*plan)[i], numofDOFs, map, x_size, y_size) && firstinvalidconf)
        {
            firstinvalidconf = 1;
            printf("ERROR: Invalid arm configuration!!!\n");
        }
    }    
    *planlength = planned_path.size();
    PQ = PQ/planned_path.size();

    std::cout<<"PATH QUALITY: "<<PQ<<"\n\n\n";

    return;
}


static void planner2(
       double*  map,
       int x_size,
       int y_size,
           double* armstart_anglesV_rad,
           double* armgoal_anglesV_rad,
     int numofDOFs,
     double*** plan,
     int* planlength)
  
{
  *plan = NULL;
  *planlength = 0;

  std::vector<double> start_pos, goal_pos;

  for (int i = 0; i < numofDOFs; i++) {
    start_pos.push_back(armstart_anglesV_rad[i]);
    goal_pos.push_back(armgoal_anglesV_rad[i]);
  }

  Node* start = new Node(start_pos, -1);
  Node* goal = new Node(goal_pos, -1);
  std::vector<Node*> RRTTree_s;
  std::vector<Node*> RRTTree_g;
  RRTTree_s.push_back(start);
  RRTTree_g.push_back(goal);

  std::vector<Node*>* treeptr;
  treeptr = &RRTTree_s;
  int iter = 0;
  double eps = PI/2;
  std::vector<double> rc(numofDOFs);
  while(1) {
     iter++;
     // std::cout<<iter<<"\n\n";
    srand(iter);
    for (int i = 0; i < numofDOFs; i++) {
      rc[i] = (rand() / (RAND_MAX/(2 * PI )));
    }
    // std::cout<<"RC CREATED\n\n\n";
    neighbor* n = nearest_neighbors(rc,*treeptr);
    // std::cout<<"NEIGHBOR\n\n\n"; 
    if(n-> distance > eps) {
      rc = new_config((*treeptr)[n->index]->v, rc, n-> distance, eps);
      n->distance = eps;
    }
    // std::cout<<"NEW CONFIG\n\n\n";
    if(extend((*treeptr)[n->index]->v, rc, n-> distance, map, x_size, y_size)){
        Node* config = new Node(rc,n->index);
        (*treeptr).push_back(config);
    // std::cout<<"EXTEND\n\n\n";
    }
    else if(!extend((*treeptr)[n->index]->v, rc, n-> distance, map, x_size, y_size)) {continue;}  
    // std::cout<<"CONNECT\n\n\n";
    if(treeptr == &RRTTree_s)
      treeptr = &RRTTree_g;
    else
      treeptr = &RRTTree_s;
    // std::cout<<"NEW NEIGHBOR\n\n\n";
    neighbor* n_new = nearest_neighbors(rc,*treeptr); 
    // std::cout<<"CONNECT\n\n\n";
    // print_config((*treeptr)[n_new->index]->v);
    // print_config(rc);
    if(extend((*treeptr)[n_new->index]->v, rc, n_new-> distance, map, x_size, y_size)){
        // std::cout<<"test0\n\n";
        Node* config = new Node(rc,n_new->index);
        // std::cout<<"test1\n\n";
        (*treeptr).push_back(config);
        // std::cout<<"test2\n\n";
        break;
    }
    if(iter%100000==0)
      break;
   
  }

  std::vector<int> planned_paths =  backtrack(RRTTree_s);
  planned_paths.pop_back();

  std::vector<int> planned_pathg =  fwdtrack(RRTTree_g);


int sz = planned_paths.size()+planned_pathg.size();
   double PQ = 0;
    int firstinvalidconf = 1;
    *plan = (double**) malloc(sz*sizeof(double*));
        for (int i = 0; i < planned_paths.size(); i++){
            (*plan)[i] = (double*) malloc(numofDOFs*sizeof(double)); 
            for(int j = 0; j < numofDOFs; j++){
              (*plan)[i][j] = RRTTree_s[planned_paths[i]] -> v[j];
            } 
            if(i>0){
                PQ+= distance(RRTTree_s[planned_paths[i-1]] -> v, RRTTree_s[planned_paths[i]] -> v);
              }             
        if(!IsValidArmConfiguration((*plan)[i], numofDOFs, map, x_size, y_size) && firstinvalidconf)
        {
            firstinvalidconf = 1;
            printf("ERROR: Invalid arm configuration!!!\n");
        }
    }
    for (int i = planned_paths.size(); i < planned_paths.size() + planned_pathg.size(); i++){
            (*plan)[i] = (double*) malloc(numofDOFs*sizeof(double)); 
            for(int j = 0; j < numofDOFs; j++){
              (*plan)[i][j] = RRTTree_g[planned_pathg[i]] -> v[j];
            }  
            if(i>0){
                PQ+= distance(RRTTree_g[planned_pathg[i-1]] -> v, RRTTree_g[planned_pathg[i]] -> v);
              }         
        if(!IsValidArmConfiguration((*plan)[i], numofDOFs, map, x_size, y_size) && firstinvalidconf)
        {
            firstinvalidconf = 1;
            printf("ERROR: Invalid arm configuration!!!\n");
        }
    }


    *planlength = sz;

        PQ = PQ/sz;

    std::cout<<"PATH QUALITY: "<<PQ<<"\n\n\n";

    return;

} 

static double RRTSrad(int v, int numofDOFs, double eps) {
    double rad = pow((1000 * log(v)/v), (1.0/numofDOFs));
    return std::min(rad, eps);
}


std::vector<neighbor *> nearest_neighbors_star(const std::vector<double>& v, const std::vector<Node*>& T, double radius)
{
  std::vector<neighbor *> ns;
  for(int i = 0; i < T.size(); i++){
    if(distance(v,T[i]->v) < radius) {
      neighbor* n = new neighbor();
      n -> distance = distance(v,T[i]->v);
      n -> index = i;
      ns.push_back(n);
    }
  }

  if(ns.empty())
    ns.push_back(nearest_neighbors(v, T));

  return ns;
}

std::vector<neighbor *> nearest_neighbors_prm(const std::vector<double>& v, const std::vector<Node*>& T, double radius)
{
  std::vector<neighbor *> ns;
  for(int i = 0; i < T.size(); i++){
    if(distance(v,T[i]->v) < radius) {
      neighbor* n = new neighbor();
      n -> distance = distance(v,T[i]->v);
      n -> index = i;
      ns.push_back(n);
    }
  }
  return ns;
}



static void planner3(
       double*  map,
       int x_size,
       int y_size,
           double* armstart_anglesV_rad,
           double* armgoal_anglesV_rad,
     int numofDOFs,
     double*** plan,
     int* planlength)
  
{

  *plan = NULL;
  *planlength = 0;

  std::vector<double> start_pos, goal_pos;

  for (int i = 0; i < numofDOFs; i++) {
    start_pos.push_back(armstart_anglesV_rad[i]);
    goal_pos.push_back(armgoal_anglesV_rad[i]);
  }

  Node* start = new Node(start_pos, -1);
  start -> cost = 0;
  Node* goal = new Node(goal_pos);
  std::vector<Node*> RRTTree;
  RRTTree.push_back(start);


  int iter = 0;
  double eps = PI/2;
  bool GOALFLAG = false;
  std::vector<double> rc(numofDOFs);
  while(1) {
    // GENERATE RANDOM CONFIGURATION
    if(iter % 4 == 0){
      rc = goal_pos;
      GOALFLAG = true;
    }
    else {
      srand(iter);
      for (int i = 0; i < numofDOFs; i++) {
        rc[i] = (rand() / (RAND_MAX/(2 * PI )));
        }
        GOALFLAG = false;
      }
      iter++;
      if(!IsValidArmConfiguration(vec_to_dbl(rc), numofDOFs, map, x_size, y_size))
            continue;
      // GET NEAREST NEIGHBORS AND NEW CONFIG
      double rrtrad  = RRTSrad(RRTTree.size(), numofDOFs, eps);
      std::vector<neighbor*> n = nearest_neighbors_star(rc,RRTTree,rrtrad); 

      double min_cost = INT_MAX;
      int min_index;

      for(int i1 = 0; i1 < n.size(); i1++) {
        if(RRTTree[n[i1] -> index] -> cost + n[i1] -> distance < min_cost) {
          min_cost = RRTTree[n[i1] -> index] -> cost + n[i1] -> distance;
          min_index = i1;
        }

      }




      if(n[min_index]-> distance > eps) {
        GOALFLAG = false;
        rc = new_config(RRTTree[n[min_index]->index]->v, rc, n[min_index]-> distance, eps);
        n[min_index]->distance = eps;
      }


      if(extend(RRTTree[n[min_index]->index]->v, rc, n[min_index]-> distance, map, x_size, y_size)){
        Node* config = new Node(rc,n[min_index]->index);
        config -> cost = RRTTree[n[min_index]->index] -> cost + 
        distance(RRTTree[n[min_index]->index] -> v, rc);
        RRTTree.push_back(config);


        for(int i2 = 0; i2 < n.size(); i2++) {
          if(i2 != min_index && RRTTree[n[i2] -> index] -> cost > config -> cost + 
            distance(config -> v, RRTTree[n[i2] -> index] -> v) ) {
          RRTTree[n[i2] -> index] -> parent = RRTTree.size() - 1;
          }
        }
        if(GOALFLAG==true)
          break;
        if(distance(rc,goal_pos)==0) break;
      }

      // if(iter%1000==0)
        // std::cout<<"ITERATION: "<<iter<<"\n";
      if(iter % 100000 == 0){
        break;
      }
  }
double PQ = 0;

  std::vector<int> planned_path =  backtrack(RRTTree);
  int firstinvalidconf = 1;
    *plan = (double**) malloc(planned_path.size()*sizeof(double*));
        for (int i = 0; i < planned_path.size(); i++){
            (*plan)[i] = (double*) malloc(numofDOFs*sizeof(double)); 
            for(int j = 0; j < numofDOFs; j++){
              (*plan)[i][j] = RRTTree[planned_path[i]] -> v[j];
            }  
                        if(i>0){
                PQ+= distance(RRTTree[planned_path[i-1]] -> v, RRTTree[planned_path[i]] -> v);
              }         
        if(!IsValidArmConfiguration((*plan)[i], numofDOFs, map, x_size, y_size) && firstinvalidconf)
        {
            firstinvalidconf = 1;
            printf("ERROR: Invalid arm configuration!!!\n");
        }
    }    
    *planlength = planned_path.size();
    
        PQ = PQ/planned_path.size();

    std::cout<<"PATH QUALITY: "<<PQ<<"\n\n\n";


    return;


}



void propnbr(const std::vector<int>& n, std::vector<Node*>* Roadmapptr, int FLAG)
{
  if (FLAG==0)
  {
    for(auto i : n){      
      if((*Roadmapptr)[i]->conn_s != 1){
        (*Roadmapptr)[i]->conn_s = 1;
        propnbr((*Roadmapptr)[i]->nbrs, Roadmapptr, 0 );
      }
    }
  }
  else if (FLAG==1)
  {
    for(auto i : n){      
      if((*Roadmapptr)[i]->conn_g != 1){
        (*Roadmapptr)[i]->conn_g = 1;
        propnbr((*Roadmapptr)[i]->nbrs, Roadmapptr, 1 );
      }
    }
  }  
}


static void planner4(
       double*  map,
       int x_size,
       int y_size,
           double* armstart_anglesV_rad,
           double* armgoal_anglesV_rad,
     int numofDOFs,
     double*** plan,
     int* planlength)
  
{

  *plan = NULL;
  *planlength = 0;

  std::vector<double> start_pos, goal_pos;

  for (int i = 0; i < numofDOFs; i++) {
    start_pos.push_back(armstart_anglesV_rad[i]);
    goal_pos.push_back(armgoal_anglesV_rad[i]);
  }

  Node* start = new Node(start_pos);
  start -> conn_s = 1;
  start -> conn_g = 0;


  Node* goal = new Node(goal_pos);
  goal -> conn_s = 0;
  goal -> conn_g = 1;
  goal ->g_val = true;



  std::vector<Node*> Roadmap;

  Roadmap.push_back(start);
  Roadmap.push_back(goal);


  int iter = 0;
  double eps = PI;
  std::vector<double> rc(numofDOFs);
  bool START=false, GOAL=false;

  while(1) {
    iter++;
    // if(iter%200000==0)
    //   break;
    // if(iter%20 == 0)
      // std::cout<<iter<<"\n\n\n";
    // GENERATE RANDOM CONFIGURATION
      srand(iter);
      for (int i = 0; i < numofDOFs; i++) {
        rc[i] = (rand() / (RAND_MAX/(2 * PI )));
        }

        if(!IsValidArmConfiguration(vec_to_dbl(rc), numofDOFs, map, x_size, y_size))
            continue;


      // std::cout<<iter<<"\n\n";      
      // GET NEAREST NEIGHBORS AND NEW CONFIG
      double rrtrad  = RRTSrad(Roadmap.size(), numofDOFs, eps);
      std::vector<neighbor*> n = nearest_neighbors_prm(rc,Roadmap,rrtrad); 
      // std::cout<<"radius: "<<n.size()<<"\n\n\n";
        Node* config = new Node(rc);
        config -> conn_s = 0;
        config -> conn_g = 0;
        Roadmap.push_back(config);

        std::vector<int> valid_nbrs;
        for(int j = 0; j < n.size(); j++) {
          if(extend(Roadmap[n[j]->index]->v, rc, n[j]-> distance, map, x_size, y_size)){
            config -> nbrs.push_back(n[j]->index);
            Roadmap[n[j]->index]->nbrs.push_back(Roadmap.size()-1);
            if(Roadmap[n[j]->index]-> conn_s == 1){
              START = 1;
              config->conn_s = 1;
            }
            if(Roadmap[n[j]->index]-> conn_g == 1){
              GOAL = 1;
              config->conn_g = 1;
            }
            valid_nbrs.push_back(j);
          }

        }

        if(START){
          propnbr(valid_nbrs, &Roadmap,0);

        }

        if(GOAL){
          propnbr(valid_nbrs, &Roadmap,1);
        }       
        std::cout<<Roadmap[1]->conn_s<<"\n\n";

        if(START && GOAL)
        {
          // std::cout<<"BREAKING AT ITER: "<<iter<<"\n\n";
          break;
        }
        START = false;
        GOAL = false;
  }

    

  int iter1 = 0;

    Roadmap[0] -> h = 1;
  
    std::queue<Node*> q;
    q.push(Roadmap[0]);
            
    Node* co;
    while(q.size() != 0) {
      iter1++;

        co = q.front();
        q.pop();
        if (co == Roadmap[1]) {
            break;
        }

        Node* neighbor;
        for(int i = 0; i < co->nbrs.size(); i++) {
            neighbor = Roadmap[co->nbrs[i]];

            if (neighbor->h == -1) {
                neighbor->pNode = co;
                neighbor->h = co->h + 1;
                q.push(neighbor);
            }
        }
    }
        *plan = (double**) malloc(co->h * sizeof(double*));
         *planlength = co->h;

double PQ = 0;

    for (int i = *planlength - 1; i >= 0; i--) {
        (*plan)[i] = (double*) malloc(numofDOFs * sizeof(double));
        for(int j = 0; j < numofDOFs; j++){
            (*plan)[i][j] = co->v[j];
        }
        if(i>0){
          PQ += distance(co->v,co->pNode->v);
        }
        co = co->pNode;

    }
          PQ = PQ / *planlength;

    std::cout<<"PATH QUALITY: "<<PQ<<"\n\n\n";

    return;

}


//prhs contains input parameters (3): 
//1st is matrix with all the obstacles
//2nd is a row vector of start angles for the arm 
//3nd is a row vector of goal angles for the arm 
//plhs should contain output parameters (2): 
//1st is a 2D matrix plan when each plan[i][j] is the value of jth angle at the ith step of the plan
//(there are D DoF of the arm (that is, D angles). So, j can take values from 0 to D-1
//2nd is planlength (int)
void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[])
     
{ 
    
    /* Check for proper number of arguments */    
    if (nrhs != 3) { 
	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidNumInputs",
                "Three input arguments required."); 
    } else if (nlhs != 2) {
	    mexErrMsgIdAndTxt( "MATLAB:planner:maxlhs",
                "One output argument required."); 
    } 
        
    /* get the dimensions of the map and the map matrix itself*/     
    int x_size = (int) mxGetM(MAP_IN);
    int y_size = (int) mxGetN(MAP_IN);
    double* map = mxGetPr(MAP_IN);
    
    /* get the start and goal angles*/     
    int numofDOFs = (int) (MAX(mxGetM(ARMSTART_IN), mxGetN(ARMSTART_IN)));
    if(numofDOFs <= 1){
	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidnumofdofs",
                "it should be at least 2");         
    }
    double* armstart_anglesV_rad = mxGetPr(ARMSTART_IN);
    if (numofDOFs != MAX(mxGetM(ARMGOAL_IN), mxGetN(ARMGOAL_IN))){
        	    mexErrMsgIdAndTxt( "MATLAB:planner:invalidnumofdofs",
                "numofDOFs in startangles is different from goalangles");         
    }
    double* armgoal_anglesV_rad = mxGetPr(ARMGOAL_IN);
        
    //call the planner
    double** plan = NULL;
    int planlength = 0;
    
    planner4(map,x_size,y_size, armstart_anglesV_rad, armgoal_anglesV_rad, numofDOFs, &plan, &planlength); 
    
    printf("planner returned plan of length=%d\n", planlength); 
    
    /* Create return values */
    if(planlength > 0)
    {
        PLAN_OUT = mxCreateNumericMatrix( (mwSize)planlength, (mwSize)numofDOFs, mxDOUBLE_CLASS, mxREAL); 
        double* plan_out = mxGetPr(PLAN_OUT);        
        //copy the values
        int i,j;
        for(i = 0; i < planlength; i++)
        {
            for (j = 0; j < numofDOFs; j++)
            {
                plan_out[j*planlength + i] = plan[i][j];
            }
        }
    }
    else
    {
        PLAN_OUT = mxCreateNumericMatrix( (mwSize)1, (mwSize)numofDOFs, mxDOUBLE_CLASS, mxREAL); 
        double* plan_out = mxGetPr(PLAN_OUT);
        //copy the values
        int j;
        for(j = 0; j < numofDOFs; j++)
        {
                plan_out[j] = armstart_anglesV_rad[j];
        }     
    }
    PLANLENGTH_OUT = mxCreateNumericMatrix( (mwSize)1, (mwSize)1, mxINT8_CLASS, mxREAL); 
    int* planlength_out = (int*)mxGetPr(PLANLENGTH_OUT);
    *planlength_out = planlength;

    
    return;
    
}